create database GestionDeAutoEcole
go 
use GestionDeAutoEcole
go 
create table Client(
CinCli varchar(50),
NomCli varchar(50),
PrenomCli varchar(50),
Sexe varchar(50),
DateNaissance date,
VilleCli varchar(50),
AdresseCli varchar(100),
TeleCli varchar(100),
Email varchar(50),
DateInscription date,
Etat varchar(50),
PhotoCli varchar(100),
constraint pkCli primary key (CinCli)
)

go 

create table Personnel(
CinPer varchar(50),
NomPer varchar(50),
PrenomPer varchar(50),
AdressePer varchar(100),
TelPer varchar(50),
EmailPer varchar(100),
DateEmbauche date,
Fonction varchar(50),
Ncnss varchar(50),
PhotoPer varchar(50),
constraint pkPer primary key (CinPer)
)

go 


create table Vehicule (
Matricule varchar(50),
Marque varchar(50),
Model varchar(50),
Carburant varchar(50),
TypeVehicule varchar(50),
constraint pkVeh primary key (Matricule)
)

go 


create table FinanceClient(
RefFinCli int identity,
CinCli varchar(50),
TypeApprentissage varchar(50),
HeurConduit int,
Prix money,
constraint pkFCli primary key (RefFinCli)
)

--table Finance Personnel (RestSalair - total)
create table FinancePersonnel(
RefFinPer int identity ,
CinPer varchar(50),
Salaire money,
DateSalaire date,
constraint pkFPer primary key (RefFinPer)
)


create table Partie(
RefPar int identity , 
RefFinCli int,
SommePar money,
DateSommePar date,
constraint pkpart primary key (RefPar)
) 


create table FinanceVehicule(
RefFinVeh int identity  ,
Matricule varchar(50),
TypeDepen varchar(100),
PaiementTypedep money ,
DateTypedep date,
CommentaireTypedep varchar(max),
constraint pkFVeh primary key (RefFinVeh)
)

create table AutreDepense (
RefAut int identity,
CinPer varchar(50),
TypeDepense varchar(100),
PaiementDepense money , 
DateDepense date ,
CommentaireDep varchar(max),
constraint pkAutreDep primary key (RefAut)
)


create table Examen (
RefExamen int identity ,
CinCli varchar(50),
Categorie varchar(50),
CinPer varchar(50),
Matricule varchar(50),
DateExamen date,
Theorique varchar(50) ,
Score int,
Pratique varchar(50) ,
ResultaFinal int,
DateRatt date,
constraint pkExam primary key (RefExamen)
)

create table Avance (
RefAvance int identity ,
RefFinPer int,
PaiementAvance money,
DateAvance date,
constraint pkave primary key (RefAvance)
);

create table Utilisateur(
Logine varchar(50),
Passeword varchar(50),
constraint pkutl primary key (logine)

);





alter table Examen add constraint FkExaCli foreign key (CinCli) 
references Client(CinCli);

alter table Examen add constraint FkExaVehi foreign key (Matricule) 
references Vehicule(Matricule);

alter table Examen add constraint FkExaPer foreign key (CinPer) 
references Personnel(CinPer);

alter table FinanceVehicule add constraint FkFinVeh_Veh foreign key (Matricule) 
references Vehicule(Matricule);

alter table FinanceClient add constraint FkFinCli_Cli foreign key (CinCLi) 
references Client(CinCLi);


alter table FinancePersonnel add constraint FkFinPer_Per foreign key (CinPer) 
references Personnel(CinPer);





alter table Partie add constraint FkParPer foreign key (RefFinCli) 
references FinanceClient(RefFinCli);

alter table AutreDepense add constraint FkAuDep foreign key (CinPer) 
references Personnel(CinPer);


alter table Avance add constraint fkAvPer foreign key (RefFinPer)
references FinancePersonnel(RefFinPer);

